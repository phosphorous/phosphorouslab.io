---
sidebar_position: 2
---

# API

How to use the developer API.

## Setup

First of all, you must set up Calibre in your development environment.

This example uses **Gradle - Kotlin DSL - Version catalog**.

:::tip

Find the current version [here](https://gitlab.com/phosphorous/calibre).

:::

`gradle/libs.version.toml`

```toml
[versions]
calibre = "[VERSION]"

[libraries]
calibre = { group = "com.gitlab.aecsocket.calibre", name = "calibre-[MODULE]", version.ref = "calibre" }
```

`build.gradle.kts`

```kt
repositories {
    // repository for the `gitlab/phosphorous` packages
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
}

dependencies {
    compileOnly(libs.calibre)
}
```

### Modules

* `calibre-core`
* `calibre-paper`
