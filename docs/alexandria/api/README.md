---
sidebar_position: 1
---

# API

How to use the developer API.

## Setup

First of all, you must set up Alexandria in your development environment.

This example uses **Gradle - Kotlin DSL - Version catalog**.

:::tip

Find the current version [here](https://gitlab.com/phosphorous/alexandria).

:::

`gradle/libs.version.toml`

```toml
[versions]
alexandria = "[VERSION]"

[libraries]
alexandria = { group = "com.gitlab.aecsocket.alexandria", name = "alexandria-[MODULE]", version.ref = "alexandria" }
```

`build.gradle.kts`

```kt
repositories {
    // repository for the `gitlab/phosphorous` packages
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
}

dependencies {
    compileOnly(libs.alexandria)
}
```

### Modules

* `alexandria-core`
* `alexandria-paper`
