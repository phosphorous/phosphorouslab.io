---
sidebar_position: 4
---

# Alexandria

[GitLab](https://gitlab.com/phosphorous/alexandria) | [Dokka](pathname:///alexandria/dokka/index.html)

---

Library for Minecraft platforms.

This is a multiplatform library containing utilities for Minecraft such as math classes, serialization etc.
Most of the documentation will be in Dokka, rather than explained in the documentation here.
