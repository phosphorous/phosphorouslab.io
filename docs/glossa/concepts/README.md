---
sidebar_position: 1
---

# Concepts

The basic concepts of Glossa.

## Translations

At its core, a **translation operation** is like a dictionary, where you look up one piece of text (the value)
based on another piece of text (the key).
With this text, **internationalization (I18N)** services provided by Glossa will do processing,
to add features like **ICU formatting** and **substitutions**.

```json
error: {
  network: "A network error occurred. Check your connection."
  generic: "This action failed. Please try again."
}
followers: {
  added: [
    "On {follow_date, date, short}, {follower} started following you."
    "You now have {total_followers, number} followers."
  ]
}
```

The blocks `{date, ...}`, `{follower}`, `{total, ...}` are **templates** - parts which are
replaced with values provided in-code: **arguments**. These templates are written as
[ICU message formats](https://icu.unicode.org/), which is an industry standard format that
is used across many different applications.

```xml
On 12/02/19, OneOfYourFollowers started following you.
You now have 184 followers.
```

:::tip

You can find a more complete guide on ICU features on the [ICU format](concepts/icu) page.
This should be read by both users and developers!

:::

:::caution

All keyed values (including message keys and argument keys) must only contain `a-z0-9_`:
* lowercase letters
* numbers
* an underscore `_`

:::

## Substitutions

Some parts of the format may be replaced with parts passed in by code, that are *not*
passed through ICU templates.

`<_key_>` defines a substitution with argument key `_key_`.

:::note

For this section, we will assume you are using Adventure/Minecraft chat components,
making it easier to demonstrate some features of substitutions.

:::

<table>

<tr>
<td>

```icu-message-format
You received the gift <gift_name>!
```

</td>
<td>

```kotlin
i18n.make("gift_received") {
  subst("gift_name", text("Fire Sword", RED))
}
```

</td>
<td>

```xml
You received the gift <red>Fire Sword</red>!
```

</td>
</tr>

</table>

Note the coloured part here, `<red>...</red>` - this wasn't in
the original message format. Instead, it came from *a chat component* being passed into
that argument `gift_name`.

:::info

For developers: In the API, for a given I18N service `I18N<T>`, the substitution
provided must be `T`. The output of `I18N.safeLine` produces a `T`, which can be put into a list argument with substitutions inside.

Glossa provides a shorthand for such an operation through the `Localizable` interface.

```kotlin
// `gift_item`: "<name> worth {value} coins"

data class GiftItem(
  val name: Component,
  val value: Int,
) : Localizable<Component> {
  override fun localize(i18n: I18N<Component>): Component = i18n.safeLine("gift_item") {
    subst("name", name)
    icu("value", value)
  }
}

val giftItem = GiftItem(text("Fire Sword", RED), 300)

i18n.safe("gift_received") {
  // no need to translate `giftItem` manually
  subst("gift_name", giftItem)
}
```

```xml
You received the gift <red>Fire Sword</red> worth 300 coins!
```

:::

## Styling tags

Similarly, MiniMessage open/closing tags can be defined to style a particular part of a message.

```json5
en-US: {
  player_health: "Your health: <health_style>{health_value, number}</health_style>"
}

styles: {
  info: { color: "gray" }
  variable: { color: "white" }
}

formats: {
  "player_health": [ "info", {
    health_style: "variable"
  } ]
}
```

```kotlin
i18n.make("player_health") {
  icu("health_value", 100.0)
}
```

```xml
<gray>Your health: <white>100.0</white></gray>
```

The tag `health_style` is not brought in from the code (`health_value`), but rather from the
format defined under the `formats` block, where `health_style` is a reference to the style `variable`.
As this style is defined as `{ color: "white" }`, the corresponding text in the tag is colored white.

:::note

You **are allowed** to have styles/substitutions and ICU arguments with the same name,
but **not** styles and substitutions with the same name.

*This is valid:*

```json5
en-US: {
  player_health: "Health: <health>{health, number}</health>"
}

formats: {
  "player_health": {
    health: "variable"
  }
}
```

```kotlin
i18n.make("player_health") {
  icu("health", 100.0)
}
```

*This is not:*

```json5
en-US: {
  player_health: "Health: <health><health></health>"
}
```

```kotlin
i18n.make("player_health") {
  subst("health", text("100.0"))
}
```

:::
