---
sidebar_position: 3
---

# Styling

Using the styling features.

:::note

These features are only available when working with Adventure components.

For developers: You must use the `MiniMessageI18N.Builder`, from the `glossa-adventure` module.

:::

Adventure components - a platform that is practically equivalent to Minecraft chat components -
can be styled by adding color, text decorations, keybind components, etc. These features are
supported in Glossa, through the use of two techniques:
* Styles/formats
* MiniMessage

## Styles

Each I18N service stores a map of keys to styles. A style defines how a component is displayed.

```json
styles: {
  info: { color: "white" }
  variable: { color: "yellow" }
  extra: { color: "gray" }
}
```

Common properties used:

| Key | Description | Examples |
|-----|-------------|----------|
| `color` | The color of text, either as a named color or as a hex value | `red`, `#ff0000` |
| `bold`, `italic`, `underline`, `strikethrough`, `obfuscated` | Toggles the respective text decoration | `true`, `false` |
| `font` | The text font, used by the client resource pack | `minecraft:uniform` |

## Formats

Each I18N service stores a map of message keys to formats. A format defines what styles are
applied to what parts of a message:

```json5
formats: {
  "server.status": [
    "info",                 // The style for the entire message
    {                       // The style for individual arguments
      uptime: "variable", // ..for the `uptime` argument
      memory: "variable"  // ..for the `memory` argument
    }
  ]
}
```

<table>

<tr>
<td>

```icu-message-format
Server uptime: {uptime, number} hours
Memory free: {memory, number} MB
```

</td>
<td>

```xml
<white>Server uptime: <yellow>5</yellow> hours</white>
<white>Memory free: <yellow>2,184</yellow> MB</white>
```

</td>
</tr>

</table>

Formats will merge from their immediate parents, so you can define a common style
for a section of translations:

```json5
styles: {
  error: { color: "red" }
  variable: { color: "yellow" }
  extra: { color: "gray" }
}

formats: {
  "error": "error" // all `error.*` messages will use the `error` style
  
  "error.permission": {      // the `error.permission` key will use the `error` style..
    "permission": "variable" // ..but its `permission` argument will be styled `variable`
  }
  
  "error.group": {                 // the `error.group` key will use the `error` style..
    "group": "variable",           // ..but its `group` arguments will be `variable`
  }
}
```

<table>

<tr>
<td>

`error.network`

</td>
<td>

```icu-message-format
A network error has occurred.
```

</td>
<td>

```xml
<red>A network error has occurred.</red>
```

</td>
</tr>

<tr></tr><tr>
<td>

`error.permission`

</td>
<td>

```icu-message-format
You do not have the permission {permission}!
```

</td>
<td>

```xml
<red>You do not have the permission <yellow>command.give</yellow>!</red>
```

</td>
</tr>

<tr></tr><tr>
<td>

`error.group`

</td>
<td>

```icu-message-format
You are not part of the group <group>.
```

</td>
<td>

```xml
<red>You are not part of the group <yellow>Owner</yellow></red>.
```

</td>
</tr>

</table>

## MiniMessage

As the final processing step, text is passed through MiniMessage,
a format for parsing text into components. This allows you to use special component types:

```icu-message-format
Press '<key:key.move_forward>' to move forwards.
```

```xml
Press '<key.move_forward>' to move forwards.
```

The `<key.(...)>` parts will be displayed by the client as the actual keybind for that action:

```
Press 'w' to move forwards.
```

ICU templates are parsed *before* MiniMessage tags, so you can do:

```icu-message-format
Press '<key:{keybind}>' to do this action.
```

```kotlin
i18n.make("do_action") {
  icu("keybind", "key.move_forward")
}
```

```xml
Press '<key.move_forward>' to do this action.
```

To see a full list of what is possible, [see the MiniMessage docs](https://docs.adventure.kyori.net/minimessage/).

:::note

MiniMessage provides support for its own styling, e.g. `<red>`, `<#ff0000>`, `<i>`, however it is
recommended to use Glossa's styling for these purposes, since it can interact with the rest of
the Glossa system.

For keybinds, etc. it is perfectly acceptable to use MiniMessage.

:::

## Addendum

<details>
<summary>List of keybinds</summary>

```json
"key.mouse.left": "Left Button",
"key.mouse.right": "Right Button",
"key.mouse.middle": "Middle Button",
"key.mouse": "Button %1$s",
"key.keyboard.unknown": "Not bound",
"key.keyboard.apostrophe": "'",
"key.keyboard.backslash": "\\",
"key.keyboard.backspace": "Backspace",
"key.keyboard.comma": ",",
"key.keyboard.delete": "Delete",
"key.keyboard.end": "End",
"key.keyboard.enter": "Enter",
"key.keyboard.equal": "=",
"key.keyboard.escape": "Escape",
"key.keyboard.f1": "F1",
"key.keyboard.f2": "F2",
"key.keyboard.f3": "F3",
"key.keyboard.f4": "F4",
"key.keyboard.f5": "F5",
"key.keyboard.f6": "F6",
"key.keyboard.f7": "F7",
"key.keyboard.f8": "F8",
"key.keyboard.f9": "F9",
"key.keyboard.f10": "F10",
"key.keyboard.f11": "F11",
"key.keyboard.f12": "F12",
"key.keyboard.f13": "F13",
"key.keyboard.f14": "F14",
"key.keyboard.f15": "F15",
"key.keyboard.f16": "F16",
"key.keyboard.f17": "F17",
"key.keyboard.f18": "F18",
"key.keyboard.f19": "F19",
"key.keyboard.f20": "F20",
"key.keyboard.f21": "F21",
"key.keyboard.f22": "F22",
"key.keyboard.f23": "F23",
"key.keyboard.f24": "F24",
"key.keyboard.f25": "F25",
"key.keyboard.grave.accent": "`",
"key.keyboard.home": "Home",
"key.keyboard.insert": "Insert",
"key.keyboard.keypad.0": "Keypad 0",
"key.keyboard.keypad.1": "Keypad 1",
"key.keyboard.keypad.2": "Keypad 2",
"key.keyboard.keypad.3": "Keypad 3",
"key.keyboard.keypad.4": "Keypad 4",
"key.keyboard.keypad.5": "Keypad 5",
"key.keyboard.keypad.6": "Keypad 6",
"key.keyboard.keypad.7": "Keypad 7",
"key.keyboard.keypad.8": "Keypad 8",
"key.keyboard.keypad.9": "Keypad 9",
"key.keyboard.keypad.add": "Keypad +",
"key.keyboard.keypad.decimal": "Keypad Decimal",
"key.keyboard.keypad.enter": "Keypad Enter",
"key.keyboard.keypad.equal": "Keypad =",
"key.keyboard.keypad.multiply": "Keypad *",
"key.keyboard.keypad.divide": "Keypad /",
"key.keyboard.keypad.subtract": "Keypad -",
"key.keyboard.left.bracket": "[",
"key.keyboard.right.bracket": "]",
"key.keyboard.minus": "-",
"key.keyboard.num.lock": "Num Lock",
"key.keyboard.caps.lock": "Caps Lock",
"key.keyboard.scroll.lock": "Scroll Lock",
"key.keyboard.page.down": "Page Down",
"key.keyboard.page.up": "Page Up",
"key.keyboard.pause": "Pause",
"key.keyboard.period": ".",
"key.keyboard.left.control": "Left Control",
"key.keyboard.right.control": "Right Control",
"key.keyboard.left.alt": "Left Alt",
"key.keyboard.right.alt": "Right Alt",
"key.keyboard.left.shift": "Left Shift",
"key.keyboard.right.shift": "Right Shift",
"key.keyboard.left.win": "Left Win",
"key.keyboard.right.win": "Right Win",
"key.keyboard.semicolon": ";",
"key.keyboard.slash": "/",
"key.keyboard.space": "Space",
"key.keyboard.tab": "Tab",
"key.keyboard.up": "Up Arrow",
"key.keyboard.down": "Down Arrow",
"key.keyboard.left": "Left Arrow",
"key.keyboard.right": "Right Arrow",
"key.keyboard.menu": "Menu",
"key.keyboard.print.screen": "Print Screen"
```

</details>
