---
sidebar_position: 2
---

# API

How to use the developer API.

## Setup

First of all, you must set up Glossa in your development environment.

This example uses **Gradle - Kotlin DSL - Version catalog**.

:::tip

Find the current version [here](https://gitlab.com/phosphorous/glossa).

:::

`gradle/libs.version.toml`

```toml
[versions]
glossa = "[VERSION]"

[libraries]
glossa = { group = "com.gitlab.aecsocket.glossa", name = "glossa-[MODULE]", version.ref = "glossa" }
```

`build.gradle.kts`

```kt
repositories {
    // repository for the `gitlab/phosphorous` packages
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
}

dependencies {
    implementation(libs.glossa)
}
```

### Modules

* `glossa-core`
* `glossa-adventure`
