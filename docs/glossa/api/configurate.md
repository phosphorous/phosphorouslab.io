---
sidebar_position: 2
---

# Configurate

Loading data into I18N builders using Configurate nodes.

## Configurate.kt

Each module which defines new I18N services holds a `Configurate.kt` file,
defining extension functions on I18N builders to load from Configurate objects.

## Translations

Use `AbstractI18N.Builder.addTranslations(ConfigurationNode)` to deserialize translations
from a `ConfigurationNode`. These methods will throw `SerializationException` if an error occurs.

```kotlin
val node = // HoconConfigurationLoader.(...)

MiniMessageI18N.Builder().apply {
  addTranslations(node)
}
```

```json5
en-US: {
  hud: {
    health: "Health: ..."
    money: "Money: ..."
    inventory: {
      carrying: "Carrying weight: ..."
      carry_max: "Max carrying weight: ..."
    }
  }
  notification: {
    overencumbered: [
      "You are carrying too much!"
      "Drop some items."
    ]
  }
}

de-DE: {
  hud: {
    health: "Health: ..."
  }
}

// `root` translations will always be the last fallback
root: {
  notification: {
    prefix: "[!] "
  }
}
```

## Styles and formats

Use:
* `MiniMessageI18N.Builder.addStyles`
* `MiniMessageI18N.Builder.addFormats`

```kotlin
MiniMessageI18N.Builder().apply {
  addStyles(node.node("styles"))
  addFormats(node.node("formats"))
}
```

```json5
styles: {
  variable: { color: "yellow" }
  error: { color: "red", bold: true }
}

formats: {
  "error": "error"
  "error.timeout": { time: "variable" }
}
```

## All from node

Use `MiniMessageI18N.Builder.addFromNode` to add all data from a node:
* Styles are added from the node at the `styles` key
* Formats are added from the node at the `formats` key
* The remaining keys are used for translations

```kotlin
MiniMessageI18N.Builder().apply {
  addFromNode(node)
}
```

## Load

Use `MiniMessageI18N.Builder.load` to load data from a `ConfigurationLoader`, using
the proper configuration options which include the correct serializers:

```kotlin
MiniMessageI18N.Builder().apply {
  load(HoconConfigurationLoader.builder()
    .file(dataFolder.resolve("lang.conf"))
    .build())
}
```
