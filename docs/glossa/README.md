---
sidebar_position: 1
---

# Glossa

[GitLab](https://gitlab.com/phosphorous/glossa) | [Dokka](pathname:///glossa/dokka/index.html)

---

ICU-based localization library written in Kotlin.

Glossa provides a way for developers to localize messages based on the popular MiniMessage format, while also
implementing the advanced localization features of Unicode's ICU library.
It is designed around Kotlin, taking advantage of its DSL features, with currently minimal support for Java -
but being a JVM language, it will run on a JVM environment.

## Features

### ICU under the hood

Unicode's [ICU4J](https://icu.unicode.org/) is a library introducing many localization features, such as:
* Plurals
* Number formatting
* Currency support

Glossa uses ICU4J to parse templates in one stage of its templating process,
so translations can use these features without compromise.

<!-- use empty <tr> in between to trick zebra striping -->

<table>

<tr>
<td>

```icu-message-format
You have {items, plural,
  one {# item}
  other {# items}
} in your cart
```

</td>
<td>

```kotlin
i18n.make("cart_items") {
  icu("items", 1)
}
```

</td>
<td>

```xml
You have 1 item in your cart
```

</td>
</tr>

<tr></tr><tr>
<td>



</td>
<td>

```kotlin
i18n.make("cart_items") {
  icu("items", 4)
}
```

</td>
<td>

```xml
You have 4 items in your cart
```

</td>
</tr>

<tr></tr><tr>
<td>

```icu-message-format
Items processed:
  {complete, number} / {total, number}
  ({percent, number, percent})
```

</td>
<td>

```kotlin
i18n.make("item_queue.processed") {
  icu("complete", 115)
  icu("total", 1000)
  icu("percent", 0.115)
}
```

</td>
<td>

English
```xml
Items processed:
  115 / 1,000
  (11.5%)
```
German
```xml
Items processed:
  115 / 1.000
  (11.5 %)
```

</td>
</tr>

<tr></tr><tr>
<td>

```icu-message-format
Health: {health, number, :: %x100 .00}
Money: {money, number, :: compact-short}
```

</td>
<td>

```kotlin
i18n.make("hud.status") {
  icu("health", 0.56789)
  icu("money", 11700)
}
```

</td>
<td>

English
```xml
Health: 56.79%
Money: 12K
```

French
```xml
Health: 56,79 %
Money: 12 k
```

</td>
</tr>

</table>

### Substitution

Insert already-generated values (such as the output of other translation operations)
into an existing translation:

<table>

<tr>
<td>

```icu-message-format
You rececived <item_name>!
```

</td>
<td>

```kotlin
import adventure.Component.*
import adventure.NamedTextColor.*

i18n.make("notification.items.received") {
  subst("item_name", text("Fire Sword", RED))
}
```

</td>
<td>

```xml
You received <red>Fire Sword</red>!
```

</td>
</tr>

</table>

From the API, you can even directly insert `Localizable` instances:

```kotlin
// store_item = "<name> worth {value, number} coins"
// store_item.name.xp_booster = "XP Booster"
// store_item.name.coin_booster = "Coin Booster"

data class StoreItem(val id: String, val value: Int) : Localizable<Component> {
  override fun localize(i18n: I18N<Component>): Component =
    i18n.safe("store_item") {
      subst("name", i18n.safe("store_item.name.$id"))
      icu("value", value)
    }
}

val xpBooster = StoreItem("xp_booster", 200)
val coinBooster = StoreItem("coin_booster", 300)
```

<table>

<tr>
<td>

```icu-message-format
You rececived <item>!
```

</td>
<td>

```kotlin
i18n.make("notification.items.received") {
  subst(xpBooster)
  // no need to localize `xpBooster` manually
}

i18n.make("notification.items.received") {
  subst(coinBooster)
}
```

</td>
<td>

```xml
You received XP Booster worth 200 coins!
```

```xml
You received Coin Booster worth 300 coins!
```

</td>
</tr>

</table>

### Designed around Kotlin

Using Kotlin means syntax is cleaner compared to Java:

```kotlin
val i18n = MiniMessageI18N.Builder().apply {
  translation(Locale.US) {
    section("listing") {
      value("posts", "Posts:")
      value("news", "News:")
    }
    section("post_info") {
      value("header",
        "{name} by {author}, published on {publish_date, date, short}",
        "{likes, plural, one {# like} other {# likes}}")
    }
  }
}.build()

i18n.make("post_info.header") {
  icu("name", "Welcome to the site!")
  icu("author", "SiteAdmin")
  icu("publish_date", Date(0))
  icu("likes", 63)
}
// > Welcome to the site! by SiteAdmin, published on 1/1/70
// > 63 likes
```

### Configurate support

[Configurate](https://github.com/spongepowered/configurate) is a library for loading configuration files.
Glossa has native support for deserializing translation and configuration files using Configurate,
through extension functions for builders in the `Configurate.kt` files.

```json5
// lang_file.conf
// The translation strings, for the locale `en-US`
en-US: {
  hello_world: "Hello world!"
}
```

```kotlin
val i18n: MiniMessageI18N = MiniMessageI18N.Builder().apply {
  load(HoconConfigurationLoader.builder()
    .file(dataFolder.resolve("lang_file.conf"))
    .build())
}.build()

val message: List<Component> = i18n.make("hello_world")
// > Hello world!
```

### Centralized styling

The MiniMessage I18N service has three features of configuration:
**translations**, **styles** and **formats**.

* **Translations** are the raw message data of the string parsed
* **Styles** are a key/value map of styles that can be used in formats.
* **Formats** define how a message, its children, and its arguments, are styled (using the style definitions provided above).

```json5
// The translation strings, for the locale `en-US`
en-US: {
  error: {
    // The MiniMessage self-closing tag `permission_node` here represents the placeholder
    no_permission: "You do not have permission '<permission_node>'!"
  }

  display: {
    // But here, the open/closing tags `health` and `armor` represent styles to apply to the text
    // The `{health, number}` and `{armor, number}` will be replaced to represent their respective values
    action_bar: "[ Health: <health>{health, number}</health> | Armor: <armor>{armor, number}</armor> ]"
  }
}

// The styles, as a key/value map
styles: {
  // Styles are defined as Adventure styles
  accent: { color: "gold" }
  error: { color: "red" }
  variable: { color: "white" }
}

formats: {
  // Any message under `error.*` will be styled as an `error`
  // (the red color is applied)
  "error": "error"

  "error.no_permission": {
    // For the argument `permission_node` in the message `error.no_permission`,
    // the argument will be styled as a `variable` (white color)
    permission_node: "variable"
  }

  // The message is styled as an `accent` (gold color)
  "display.action_bar": [ "accent", {
    // With tag `health` being styled as a `variable`...
    health: "variable"
    // ...as well as the tag `armor`.
    armor: "variable"
  } ]
}
```

<table>

<tr>
<td>

```kotlin
i18n.make("error.no_permission") {
  subst("permission_node", text("the.permission.node"))
}
```

</td>
<td>

```xml
<red>You do not have permission '<white>the.permission.node</white>'!</red>
```

</td>
</tr><tr></tr>

<tr>
<td>

```kotlin
i18n.make("display.action_bar") {
  icu("health", 18.5)
  icu("armor", 5.2)
}
```

</td>
<td>

```xml
<gold>[ Health: <white>18.5</white> | Armor: <white>5.2</white> ]</gold>
```

</td>
</tr>

</table>

### MiniMessage and ICU integration

Due to the design of the formatter, ICU templates can seamlessly integrate with MiniMessage tags,
allowing you to create good looking messages which can still be translated easily.

```json5
styles: {
  info: { color: "gray" }
  variable: { color: "white" }
}

formats: {
  "command": "info"
  "command.reload": {
    messages: "variable"
  }
}
```

```icu-message-format
Reloaded with {messages, plural,
  =0 {no messages.}
  one {<messages>#</messages> message.}
  other {<messages>#</messages> messages.}
}
```

<table>

<tr></tr><tr>
<td>Source</td><td>Intermediate</td><td>Result</td>
</tr>

<tr>
<td>

```kotlin
i18n.make("command.reload") {
  icu("messages", 0)
}
```

</td>
<td>

```xml
Reloaded with no messages.
```

</td>
<td>

```xml
<gray>Reloaded with no messages.</gray>
```

</td>
</tr>

<tr></tr><tr>
<td>

```kotlin
i18n.make("command.reload") {
  icu("messages", 1)
}
```

</td>
<td>

```xml
Reloaded with <messages>1</messages> message.
```

</td>
<td>

```xml
<gray>Reloaded with <white>1</white> message.</gray>
```

</td>
</tr>

<tr></tr><tr>
<td>

```kotlin
i18n.make("command.reload") {
  icu("messages", 4)
}
```

</td>
<td>

```xml
Reloaded with <messages>4</messages> messages.
```

</td>
<td>

```xml
<gray>Reloaded with <white>4</white> messages.</gray>
```

</td>
</tr>

</table>
