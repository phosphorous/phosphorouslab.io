---
sidebar_position: 2
---

# KtRuntime

[GitLab](https://gitlab.com/phosphorous/kt-runtime) | [Dokka](pathname:///kt-runtime/dokka/index.html)

---

Kotlin stdlib runtime support for servers.

This library provides developers with:
* `kotlin-stdlib`
* `kotlin-reflect`
* `kotlinx-coroutines-core`

No need to shade Kotlin or add it to the library loader.

This library is used in all Phosphorous Kotlin-dependent plugins. 

:::note

The version of the `kt-runtime` module is equivalent to the verison of Kotlin
that it bundles (however the extension libraries will have a different version, see below).

:::

## Use

This library supports:
* Spigot
* Paper

...and does **not** have to be added as a dependency into your plugins.
Just add it as a dependency in your `plugin.yml`:

```yml
depend:
  - "KtRuntime"
```

And add the Kotlin dependencies that you want as `compileOnly` to your buildscript:

```kotlin
dependencies {
    compileOnly("org.jetbrains.kotlin", "kotlin-reflect", "1.7.20")
    compileOnly("org.jetbrains.kotlinx", "kotlin-coroutines-core", "1.6.4")
}
```

## Versions

* `1.7.10`:
  * `kotlinx-coroutines-core`: `1.6.4`
* `1.7.20`:
  * `kotlinx-coroutines-core`: `1.6.4`
