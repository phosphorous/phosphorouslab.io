---
sidebar_position: 3
---

# Example config

Instructions on installing a working Sokol example with physics, for 1.19.3.

## Download

Download the latest versions of all of these (for the plugins, without mojmap):

* [Paper server](https://papermc.io/downloads)
* [KtRuntime](https://gitlab.com/phosphorous/kt-runtime)
* [CraftBullet](https://gitlab.com/phosphorous/craftbullet)
* [Alexandria](https://gitlab.com/phosphorous/alexandria)
* [Sokol](https://gitlab.com/phosphorous/sokol)

Then download these examples:

* [Sokol configuration](https://www.dropbox.com/s/hwtk9ki0ms86clj/sokol-test-configuration.zip?dl=1)
* [Resource pack](https://www.dropbox.com/s/euw40qstds1ppdn/sokol-test-resource-pack.zip?dl=1)

## Instructions

### Server

1. Create a folder to store the server in (referred to as `root` throughout).
2. Place the Paper server `.jar` file into `root`.
3. Extract the contents of `sokol-test-configuration.zip` into `root`.
  * By the end, there should be a folder `plugins` with another folder `Sokol` inside.
4. Move all of the plugin `.jar` files into the `plugins` folder.
5. Double-click the `.jar` file and wait for the server to start.
6. Set `eula=true` in `eula.txt` and start the server again.

### Client

1. Place the `sokol-test-resource-pack.zip` into your Minecraft resource pack directory.
  * Follow [this guide](https://minecraft.fandom.com/wiki/Tutorials/Loading_a_resource_pack#Windows_(Java_Edition)) for instructions.
2. Open Minecraft and enable the resource pack above.
3. Connect to `localhost`.

### In-game

1. In your server console, run `/op <your username>` to make yourself an operator.
2. Run the command `/sokol summon ~ ~ ~ 1 debug_cube` to spawn a test physics cube at your current position.
  * You can change the position by changing the `~ ~ ~` and amount of cubes by changing the `1`.
3. Hover over a cube and right-click (with any item in your hand) to pick it up and let it go.
4. Run the command `/craftbullet launcher -f 10 -m 1` to give yourself an item which can launch cubes.
  * Left or right click with that item in hand to launch a cube from your eyes.
  * Change `-f [number]` to change the launch force (in Newtons), and `-m [number]` to change the mass (in kilograms).
5. Run the command `/kill @e[type=armor_stand]` to remove all cubes.
