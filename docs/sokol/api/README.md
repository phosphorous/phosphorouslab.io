---
sidebar_position: 2
---

# API

How to use the developer API.

## Setup

First of all, you must set up Sokol in your development environment.

This example uses **Gradle - Kotlin DSL - Version catalog**.

:::tip

Find the current version [here](https://gitlab.com/phosphorous/sokol).

:::

`gradle/libs.version.toml`

```toml
[versions]
sokol = "[VERSION]"

[libraries]
sokol = { group = "com.gitlab.aecsocket.sokol", name = "sokol-[MODULE]", version.ref = "sokol" }
```

`build.gradle.kts`

```kt
repositories {
    // repository for the `gitlab/phosphorous` packages
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
}

dependencies {
    implementation(libs.sokol)
}
```

### Modules

* `sokol-core`
* `sokol-paper`
