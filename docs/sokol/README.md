---
sidebar_position: 5
---

# Sokol

[GitLab](https://gitlab.com/phosphorous/sokol) | [Dokka](pathname:///sokol/dokka/index.html)

---

Entity composition and management framework.

Sokol is an implementation of an [entity-component-system](https://en.wikipedia.org/wiki/Entity_component_system),
inspired by [Artemis](https://github.com/junkdog/artemis-odb/) and similar frameworks. The ECS pattern
provides developers and server admins the freedom to create scalable custom entities defined by
a data-driven composition approach, rather than the typical approach of one item correlating to one
function.

Entities in this context are defined as anything that can store state within a Minecraft world - be it
an item, block, or mob. Any of these can take in events from the world - such as a player right-clicking
with an item in hand - and in turn, run actions on the world and modify itself.

## Features

### Hosts

Each entity that can store state is referred to as a **host**, in the sense that it can host the
**entity** which defines how it responds to events. Many parts of a game world can act as hosts, such as:
* Minecraft entities (e.g. animals, players)
* Stateful blocks (e.g. chests, furnaces)
* Items

These hosts can take in events from the world:
* A player right-clicks with an item in hand
* A block is broken
* A single game tick occurs (20 times per second)
  * Note: this includes every loaded item in the game - any items in chests, shulkers, entities, etc. will be ticked

In turn, these hosts pass these events down to the entity, which then acts on these events.

### Entity-component-system

* **Entities are an identifier.** They run no logic nor store data, but only act as a logical container for components.
* **Components are data.** They exist as part of an entity, and run no logic, but store stateful data.
* **Systems are logic.** They run code on a set of components, on all entities in the world.

Entities are created at runtime whenever they are needed - for example, each mob and each item stack in the world
is an entity.

Components are stored on entities, and each component is constructed from a **component profile** defined by the
server administrator.
* The **component** stores instanced, persisted data, which is unique for each entity in the world.
* The **component profile** stores configured data, which is shared for all entities which share the same **entity profile**.

Systems are pieces of code that are executed on all entities with a specific set of components. These are defined
by Sokol itself, and any Sokol-dependent plugins. These act on the data in components, but store no data (or configuration)
themselves.

### Composite

Inbuilt into Sokol is the composite system, which allows entities to store other entities under themselves as children.
This allows creating a tree structure of entities, which can be forwarded events and interact with components.
These trees can be as complex as you like, allowing you to make items akin to Escape from Tarkov's complex
gun trees:

![M4A1 build from Escape from Tarkov](https://webf-store.escapefromtarkov.com/uploads/monthly_2018_02/5a92d3980294b_2018-02-2510-12(0).png.9a0fe484b2fa2daebb715d51265896f1.png)

* M4A1 receiver
  * Colt buffer tube
    * MOE Stock
  * OMRG pistol grip
  * M4A1 upper receiver
    * 260mm barrel
      * Hybrid 46 suppressor
    * LVOA-S handguard
      * LS321 laser sight

and so on...

## Compared to other libraries

Sokol is not designed to be a drop-in replacement for other plugins such as Oraxen or ItemAdder.
Sokol's scope is not limited to items (entities can be stored on anything that holds persistent
data, such as worlds themselves), and the default features provided in Sokol are not designed for admins
to configure items to do functions like summon lightning.

Instead, Sokol has the focus of being a barebones library for other mods and plugins to expand on,
allowing these plugins to define features which in turn can be used by server admins to set up their
custom items, etc.

Sokol was originally part of a larger plugin, [Calibre](https://gitlab.com/phosphorous/calibre) -
a modular gun plugin - however was split off to maintain the "custom behaviour" part independently. To
see an example of how Sokol can be used to design functional custom items, see Calibre.
