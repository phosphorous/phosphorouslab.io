---
sidebar_position: 2
---

# Extras

Extra game-specific features on top of Bullet.

## Explosions

Any explosions in the world will also apply forces to nearby physics objects.
You can create your own explosion via `ServerPhysicsSpace.explode`.
