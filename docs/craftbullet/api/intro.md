---
sidebar_position: 1
---

# Intro

Introduction to the API.

:::info

The Dokka documentation is available from [the external Dokka link here](pathname:///craftbullet/dokka/index.html).

:::

## Kotlin

CraftBullet is written in Kotlin, however it is still possible to use in a Java environment.
Code in this documentation will be written in Kotlin, but can be translated to Java.

Extension methods are also used extensively throughout the project, especially for Bullet <-> Bukkit
spatial type conversions. These can be translated to Java:

```kotlin
var Entity.mass: Float

myEntity.mass = 10f
```

```java
ExtensionsKt.setMass(myEntity, 10f)
```

## Instance

The main `CraftBullet` instance can be accessed through the `CraftBulletAPI` value (not the interface).

```kotlin
val api: CraftBullet = CraftBulletAPI
doStuff(api)
```

In Java, a utility method can be used instead:
```java
CraftBullet api = CraftBullet.instance();
doStuff(api);
```

## Physics thread

One of the most important parts to note is that *all physics operations must be done on the physics thread.*
To maintain decent performance, CraftBullet runs physics in a separate off-main thread, which means for
writing operations to the physics state, you must schedule a task on the physics thread.

If you do not run physics operations on this thread, **the entire server may segfault!**

```kotlin
CraftBulletAPI.executePhysics {
  doPhysicsStuff()
}
```

## Physics space

All physics objects are stored in a Bullet PhysicsSpace. CraftBullet automatically creates a physics space for each
world, which can be accessed using `CraftBullet.spaceOf(World): ServerPhysicsSpace`. This will create an empty space if none exists for
this world.

Physics worlds are infinite in size, and have a floor plane at Y=-128. Their settings can be modified through
the configuration file `settings.conf`.

## Physics objects

Physics objects can be added to a physics space just like any other Bullet application, using `PhysicsSpace.addCollisionObject`.

To keep track of your objects, a `ServerPhysicsSpace` stores a `Map<UUID, TrackedPhysicsObject>` which can be used to key your
objects. You can add a tracked object by implementing `TrackedPhysicsObject`, providing it a UUID (`UUID.randomUUID()` will work fine)
and calling `addCollisionObject` (there is no special handling for adding an object.)

To remove a tracked object, either call `removeCollisionObject` with the `TrackedPhysicsObject` instance, or `removeTracked(UUID)`.

To get a tracked object by its ID, use `trackedBy(UUID): TrackedPhysicsObject?`.

## Entity bodies

All entities spawned in the world, by default, are given a collision body, unless either:
* the collision whitelist is not empty AND the entity type is not in the whitelist
* the entity type is in the collision blacklist
* the entity is a player in spectator mode
* the entity has the `craftbullet:no_collision` NBT tag

To access the entity body for an entity, you can access it by `ServerPhysicsSpace.trackedBy(entityUuid)`.
Note that the body may not exist.

Players are automatically given a capsule collision shape, and all other entities are given a box shape which rotates with the entity.

CraftBullet defines extension methods on Entity to manipulate the body, notably:
* `val Entity.collisionBody: PaperEntityPhysicsObject?`
* `fun Entity.executePhysics(task: (PaperEntityPhysicsObject) -> Unit)`
* `var Entity.hasCollision: Boolean`
* `var Entity.mass: Float`

## Hooks

You can hook into the physics step loop in various places. This is important if you need to run
physics logic on each update. Note that all hooks will run on the physics thread.

"Pre-step" refers to code running before the physics space update. "Post-step" refers to code
running after the update.

* Implement `TrackedPhysicsObject` and override `preStep` or `postStep`
  * This is called before/after the space that this object is in is stepped
* `CraftBulletAPI.onPreStep` and `CraftBulletAPI.onPostStep`
  * This is called before/after *all* spaces are stepped
