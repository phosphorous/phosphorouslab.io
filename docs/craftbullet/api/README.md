---
sidebar_position: 2
---

# API

How to use the developer API.

## Setup

First of all, you must set up CraftBullet in your development environment.

This example uses **Gradle - Kotlin DSL - Version catalog**.

:::tip

Find the current version [here](https://gitlab.com/phosphorous/craftbullet).

:::

`gradle/libs.version.toml`

```toml
[versions]
craftbullet = "[VERSION]"

[libraries]
craftbullet = { group = "com.gitlab.aecsocket.craftbullet", name = "craftbullet-[MODULE]", version.ref = "craftbullet" }
```

`build.gradle.kts`

```kt
repositories {
    // repository for the `gitlab/phosphorous` packages
    maven("https://gitlab.com/api/v4/groups/9631292/-/packages/maven")
}

dependencies {
    compileOnly(libs.craftbullet)
}
```

### Modules

* `craftbullet-core`
* `craftbullet-paper`
