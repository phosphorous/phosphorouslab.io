---
sidebar_position: 1
---

# Command

You can use the `/craftbullet` command in-game for debugging.

## `reload`

Permission: `craftbullet.command.reload`

Reloads all data for the plugin, such as the settings file.

## `stats`

Permission: `craftbullet.command.stats`

Prints stats on the current state of the physics engine, including:
* How many objects are currently in each physics space
* How long it takes to update the physics state

## `mass`

Permission: `craftbullet.command.mass`

Arguments:
* `target`: entity selector - entity to apply to
* `(mass)`: decimal - new mass to set to the entity (kilograms)

Reads or writes the (custom) mass of an entity. If `mass` is provided, sets the mass of the entity.
Otherwise, prints out its current mass and default mass.

## `collision`

Permission: `craftbullet.command.collision`

Arguments:
* `target`: entity selector - entity to apply to
* `(collision)`: boolean - new collision state to set to the entity

Enables or disables the collision state of an entity. If `collision` is provided, sets the collision state.
Otherwise, prints out its current collision state.

## `draw`

Permission: `craftbullet.command.draw`

Flags:
* `--com`, `-c` - draw the center of mass of bodies
* `--velocity`, `-v` - draw the velocity of bodies
* `--shape`, `-s` - draw the collision shape of bodies

Toggles drawing the physics state of nearby objects to your player, including the center of mass
and collision shape.

## `launcher`

Permission: `craftbullet.command.launcher`

Flags:
* `--force`, `-f`: decimal - force, in Newtons, to launch the body forwards
* `--mass`, `-m`: decimal - mass, in kilograms, of the body
* `--friction`, `-r`: decimal - friction coefficient of the body (0 to 1)

Gives your player an item which allows launching debug boxes.
The box launched has half extents `(0.5, 0.5, 0.5)` and is spawned from your eye location,
in your eye direction. Left or right click with this item in your hand to launch the box.

The appearance of the box can be customized in `settings.conf`.
