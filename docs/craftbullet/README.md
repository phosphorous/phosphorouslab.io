---
sidebar_position: 3
---

# CraftBullet

[GitLab](https://gitlab.com/phosphorous/craftbullet) | [Dokka](pathname:///craftbullet/dokka/index.html)

---

Bullet rigid body physics adapter for servers.

This library provides the core utilities for developers to add physics objects to a world:
* Backed by the [Bullet](https://github.com/bulletphysics/bullet3) physics engine
* Using the [Libbulletjme](https://github.com/stephengold/Libbulletjme) Java bindings
* With parts inspired by the [Rayon](https://github.com/LazuriteMC/Rayon) Minecraft mod.

CraftBullet handles collisions with your own objects, as well as with the game world, and provides
extra debug functionality such as drawing out collision shapes in the world. It aims to have the least
performance impact possible on your main thread, by offloading physics calculations to a separate thread.

## What's missing

* Physics objects pushing world entities
* Players colliding with physics objects
* Water buoyancy
